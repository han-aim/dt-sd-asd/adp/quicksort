#![deny(clippy::all)]
#![deny(clippy::pedantic)]
#![allow(clippy::all)]
#![allow(clippy::needless_return)]
#![feature(is_sorted)]

pub mod hashing;
pub mod sorting;

use core::{cmp::Ordering, fmt};

/// Mediaan-van-drie pivotselectie.
pub fn spil<Item, const AFLOPEND: bool>(rij: &mut [Item]) -> usize
where
    Item: Ord + PartialOrd + Clone,
{
    // `saturating_sub` trekt de waarde af (subtract) tot het minimum van het type.
    // Dus kan niet negatief worden of underflowen, immers een lengte is altijd 0 of hoger.
    let index_laatste = rij.len().saturating_sub(1);
    // Index van het midden van de rij (afgekapt naar beneden).
    let index_middelste = index_laatste.saturating_div(2);
    if index_laatste < 2 {
        return 0;
    }
    if (AFLOPEND && rij[index_laatste] > rij[0]) || (!AFLOPEND && rij[index_laatste] < rij[0]) {
        rij.swap(0, index_laatste);
    }
    if (AFLOPEND && rij[index_middelste] > rij[0]) || (!AFLOPEND && rij[index_middelste] < rij[0]) {
        rij.swap(index_middelste, 0);
    }
    if (AFLOPEND && rij[index_laatste] > rij[index_middelste])
        || (!AFLOPEND && rij[index_laatste] < rij[index_middelste])
    {
        rij.swap(index_laatste, index_middelste);
    }
    // Het middelste element is nu tevens de mediaan.
    return index_middelste;
}

/// Dijkstra's driewegspartitionering.
pub fn partitioneer_driewegs<Item, const AFLOPEND: bool>(
    rij: &mut [Item],
    index_waarde_middelste: usize,
) -> (usize, usize)
where
    Item: Ord + PartialOrd + Clone + fmt::Debug,
{
    let mut index_deelrij_linker_laatste = 0;
    let mut index_deelrij_rechter_eerste = rij.len().saturating_sub(1);
    let middelste = rij[index_waarde_middelste].clone();
    let mut index_nu = 0;
    if AFLOPEND {
        while index_nu <= index_deelrij_rechter_eerste {
            match rij[index_nu].cmp(&middelste) {
                Ordering::Equal => {
                    index_nu += 1;
                }
                Ordering::Less => {
                    rij.swap(index_nu, index_deelrij_rechter_eerste);
                    index_deelrij_rechter_eerste -= 1;
                }
                Ordering::Greater => {
                    rij.swap(index_deelrij_linker_laatste, index_nu);
                    index_deelrij_linker_laatste += 1;
                    index_nu += 1;
                }
            }
        }
    } else {
        while index_nu <= index_deelrij_rechter_eerste {
            match rij[index_nu].cmp(&middelste) {
                Ordering::Equal => {
                    index_nu += 1;
                }
                Ordering::Greater => {
                    rij.swap(index_nu, index_deelrij_rechter_eerste);
                    index_deelrij_rechter_eerste -= 1;
                }
                Ordering::Less => {
                    rij.swap(index_deelrij_linker_laatste, index_nu);
                    index_deelrij_linker_laatste += 1;
                    index_nu += 1;
                }
            }
        }
    }
    return (index_deelrij_linker_laatste, index_deelrij_rechter_eerste);
}

/// Onstabiele, in-place, recursieve, mediaan-van-drie driewegs Quicksort.
pub fn quicksort<Item, const AFLOPEND: bool>(rij: &mut [Item])
where
    Item: Ord + PartialOrd + Clone + fmt::Debug,
{
    dbg!(&rij);
    if rij.len() > 2 {
        let index_element_spil = spil::<_, AFLOPEND>(rij);
        let (index_deelrij_linker_laatste, index_deelrij_rechter_eerste) =
            partitioneer_driewegs::<_, AFLOPEND>(rij, index_element_spil);
        let (deelrij_linker, deelrij_rest) = rij.split_at_mut(index_deelrij_linker_laatste);
        let (_, deelrij_rechter) = deelrij_rest.split_at_mut(
            index_deelrij_rechter_eerste.saturating_sub(index_deelrij_linker_laatste),
        );
        quicksort::<_, AFLOPEND>(deelrij_linker);
        quicksort::<_, AFLOPEND>(deelrij_rechter);
    } else if rij.len() == 2 && ((AFLOPEND && rij[0] < rij[1]) || (!AFLOPEND && rij[0] > rij[1])) {
        // De rij is nul, één of twee lang.
        rij.swap(0, 1);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use proptest::prelude::*;
    const AFLOPEND_1: [i16; 10] = [782, 99, 83, 65, 4, 2, 2, 1, 0, -31];
    const AFLOPEND_2: [i16; 2] = [1, 0];
    const AFLOPEND_3: [i16; 4] = [3, 2, 1, 0];
    const AFLOPEND_4: [i16; 3] = [3, 2, 1];
    const OPLOPEND_1: [i16; 10] = [-31, 0, 1, 2, 2, 4, 65, 83, 99, 782];
    const OPLOPEND_2: [i16; 2] = [0, 1];
    const OPLOPEND_3: [i16; 4] = [0, 1, 2, 3];
    const OPLOPEND_4: [i16; 3] = [1, 2, 3];
    const ONGESORTEERD_1: [i16; 10] = [4, 65, 2, -31, 0, 99, 2, 83, 782, 1];
    const ONGESORTEERD_2: [i16; 2] = [0, 1];
    const ONGESORTEERD_3: [i16; 4] = [0, 1, 2, 3];
    const ONGESORTEERD_4: [i16; 3] = [3, 2, 1];
    const ONGESORTEERD_5: [i16; 2] = [3, 3];
    const ONGESORTEERD_6: [i16; 3] = [3, 3, 3];
    const ONGESORTEERD_7: [i16; 8] = [1, 2, 1, 3, 3, 4, 3, 4];
    const LEEG: [bool; 0] = [];
    const LETTERS: [char; 14] = [
        'P', 'A', 'B', 'X', 'W', 'P', 'P', 'V', 'P', 'D', 'P', 'C', 'Y', 'Z',
    ];

    /// Eigenschap-gebaseerd testen.
    ///
    /// Generereert intelligent dynamische rijen met getallen, sorteert die met deze Quicksort, en controleert of de rijen volgens de Rust standard library (`is_sorted`) correct gesorteerd zijn.
    pub mod proptests {
        use super::*;
        proptest! {
            #[test]
            fn test_quicksort_oplopend(mut rij_dynamisch in prop::collection::vec(0..usize::max_value(), 1..100)) {
                quicksort::<_, false>(&mut rij_dynamisch);
                prop_assert!(rij_dynamisch.is_sorted());
            }

            #[test]
            fn test_quicksort_aflopend(mut rij_dynamisch in prop::collection::vec(0..usize::max_value(), 1..100)) {
                quicksort::<_, true>(&mut rij_dynamisch);
                rij_dynamisch.reverse();
                prop_assert!(rij_dynamisch.is_sorted());
            }
        }
    }

    pub mod partitioneer_driewegs {
        use super::*;

        /// Oplopend, letters.
        #[test]
        fn test_a() {
            let mut letters = LETTERS;
            partitioneer_driewegs::<_, false>(&mut letters, 0);
            assert_eq!(
                letters,
                ['A', 'B', 'C', 'D', 'P', 'P', 'P', 'P', 'P', 'V', 'W', 'Y', 'Z', 'X']
            );
        }

        /// Aflopend, letters.
        #[test]
        fn test_b() {
            let mut letters = LETTERS;
            partitioneer_driewegs::<_, true>(&mut letters, 0);
            assert_eq!(
                letters,
                ['Z', 'Y', 'X', 'W', 'V', 'P', 'P', 'P', 'P', 'P', 'C', 'D', 'B', 'A']
            );
        }

        #[test]
        fn test_1() {
            // ONGESORTEERD_1;
            let mut ongesorteerd_1 = [4, 65, 2, -31, 0, 99, 2, 83, 782, 1];
            partitioneer_driewegs::<_, false>(&mut ongesorteerd_1, 4);
            // [4, 65, 2, -31, 0, 99, 2, 83, 782, 1];
            assert_eq!(ongesorteerd_1, [-31, 0, 2, 65, 99, 2, 83, 782, 1, 4]);
        }

        #[test]
        fn test_1b() {
            let mut ongesorteerd_1 = [1, 1, 2, 0, 1, 2, 0, 1, 0, 2, 1];
            partitioneer_driewegs::<_, false>(&mut ongesorteerd_1, 4);
            assert_eq!(ongesorteerd_1, [0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2]);
        }

        #[test]
        fn test_1c() {
            let mut ongesorteerd_1 = [1, 1, 2, 0, 1, 2, 0, 1, 0, 2, 1];
            partitioneer_driewegs::<_, true>(&mut ongesorteerd_1, 4);
            assert_eq!(ongesorteerd_1, [2, 2, 2, 1, 1, 1, 1, 1, 0, 0, 0]);
        }

        #[test]
        fn test_2() {
            let mut ongesorteerd_2 = ONGESORTEERD_7;
            partitioneer_driewegs::<_, false>(&mut ongesorteerd_2, 3);
            assert_eq!(ongesorteerd_2, [1, 2, 1, 3, 3, 3, 4, 4]);
        }

        #[test]
        fn test_3() {
            let mut ongesorteerd_3 = ONGESORTEERD_5;
            partitioneer_driewegs::<_, false>(&mut ongesorteerd_3, 0);
            assert_eq!(ongesorteerd_3, [3, 3]);
        }

        #[test]
        fn test_4() {
            let mut ongesorteerd_4 = ONGESORTEERD_6;
            partitioneer_driewegs::<_, false>(&mut ongesorteerd_4, 1);
            assert_eq!(ongesorteerd_4, [3, 3, 3]);
        }

        /// Index van middelste is ongeldig.
        #[test]
        #[should_panic]
        fn test_5() {
            let mut rij_leeg = LEEG;
            partitioneer_driewegs::<_, false>(&mut rij_leeg, 123);
            assert_eq!(rij_leeg, []);
        }
    }

    pub mod spil {
        use super::*;

        #[test]
        fn test_1() {
            let rij = &mut [1, 2, 3];
            let spil_1 = spil::<_, false>(rij);
            dbg!(&rij, spil_1);
            assert_eq!(spil_1, 1);
            assert_eq!(*rij, [1, 2, 3]);
        }

        #[test]
        fn test_2() {
            let rij = &mut [3, 2, 1];
            let spil_1 = spil::<_, false>(rij);
            dbg!(&rij, spil_1);
            assert_eq!(spil_1, 1);
            assert_eq!(*rij, [1, 2, 3]);
        }

        #[test]
        fn test_3() {
            let rij = &mut [1, 3, 2];
            let spil_1 = spil::<_, false>(rij);
            dbg!(&rij, spil_1);
            assert_eq!(spil_1, 1);
            assert_eq!(*rij, [1, 2, 3]);
        }

        #[test]
        fn test_4() {
            let rij = &mut [3, 1, 2];
            let spil_1 = spil::<_, false>(rij);
            dbg!(&rij, spil_1);
            assert_eq!(spil_1, 1);
            assert_eq!(*rij, [1, 2, 3]);
        }

        #[test]
        fn test_5() {
            let rij = &mut [3, 1, 2];
            let spil_1 = spil::<_, true>(rij);
            dbg!(&rij, spil_1);
            assert_eq!(spil_1, 1);
            assert_eq!(*rij, [3, 2, 1]);
        }

        #[test]
        fn test_5b() {
            let rij = &mut [3, 2, 2];
            let spil_1 = spil::<_, true>(rij);
            dbg!(&rij, spil_1);
            assert_eq!(spil_1, 1);
            assert_eq!(*rij, [3, 2, 2]);
        }

        #[test]
        fn test_5c() {
            let rij = &mut [2, 3, 2];
            let spil_1 = spil::<_, true>(rij);
            dbg!(&rij, spil_1);
            assert_eq!(spil_1, 1);
            assert_eq!(*rij, [3, 2, 2]);
        }

        #[test]
        fn test_6() {
            let rij = &mut [-12, 3, 1, 1, 2, 1, -10];
            let spil_1 = spil::<_, false>(rij);
            dbg!(&rij, spil_1);
            assert_eq!(spil_1, 3);
            assert_eq!(*rij, [-12, 3, 1, -10, 2, 1, 1]);
        }

        #[test]
        fn test_7() {
            let rij = &mut [4, 65, 2, -31, 0, 99, 2, 83, 782, 1];
            let spil_1 = spil::<_, false>(rij);
            dbg!(&rij, spil_1);
            assert_eq!(spil_1, 4);
            assert_eq!(*rij, [0, 65, 2, -31, 1, 99, 2, 83, 782, 4]);
        }
    }

    pub mod quicksort {
        use super::*;

        #[test]
        fn test_0() {
            let mut unsorted = [0, 1, 0, 1];
            quicksort::<_, false>(&mut unsorted);
            assert_eq!(unsorted, [0, 0, 1, 1]);
        }

        #[test]
        fn test_01() {
            let mut unsorted = [2, 2, 4];
            quicksort::<_, false>(&mut unsorted);
            assert_eq!(unsorted, [2, 2, 4]);
        }

        #[test]
        fn test_1() {
            let mut unsorted = ONGESORTEERD_1;
            quicksort::<_, false>(&mut unsorted);
            assert_eq!(unsorted, OPLOPEND_1);
        }

        #[test]
        fn test_2() {
            let mut unsorted = ONGESORTEERD_2;
            quicksort::<_, false>(&mut unsorted);
            assert_eq!(unsorted, OPLOPEND_2);
        }

        #[test]
        fn test_3() {
            let mut unsorted = ONGESORTEERD_3;
            quicksort::<_, false>(&mut unsorted);
            assert_eq!(unsorted, OPLOPEND_3);
        }

        #[test]
        fn test_4() {
            let mut unsorted = ONGESORTEERD_4;
            quicksort::<_, false>(&mut unsorted);
            assert_eq!(unsorted, OPLOPEND_4);
        }

        #[test]
        fn test_5() {
            let mut unsorted = ONGESORTEERD_1;
            quicksort::<_, true>(&mut unsorted);
            assert_eq!(unsorted, AFLOPEND_1);
        }

        #[test]
        fn test_123() {
            let mut unsorted = [1, 2, 3, 1, 2, 3];
            quicksort::<_, false>(&mut unsorted);
            assert_eq!(unsorted, [1, 1, 2, 2, 3, 3]);
        }

        #[test]
        fn test_6() {
            let mut unsorted = ONGESORTEERD_2;
            quicksort::<_, true>(&mut unsorted);
            assert_eq!(unsorted, AFLOPEND_2);
        }

        #[test]
        fn test_7() {
            let mut unsorted = ONGESORTEERD_3;
            quicksort::<_, true>(&mut unsorted);
            assert_eq!(unsorted, AFLOPEND_3);
        }

        #[test]
        fn test_8() {
            let mut unsorted = ONGESORTEERD_4;
            quicksort::<_, true>(&mut unsorted);
            assert_eq!(unsorted, AFLOPEND_4);
        }

        #[test]
        fn test_9() {
            let mut unsorted = [253, 44];
            quicksort::<_, true>(&mut unsorted);
            assert_eq!(unsorted, [253, 44]);
        }
    }
}
