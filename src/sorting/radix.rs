// Bron: https://rosettacode.org/wiki/Sorting_algorithms/Radix_sort#Rust
/// LSD Radix sort met binaire radix.

fn merge(rij_invoer_1: &[i32], rij_invoer_2: &[i32], rij_uitvoer: &mut [i32]) {
    let (deelrij_links, deelrij_rechts) = rij_uitvoer.split_at_mut(rij_invoer_1.len());
    deelrij_links.clone_from_slice(rij_invoer_1);
    deelrij_rechts.clone_from_slice(rij_invoer_2);
}

fn radix_sort(rij: &mut [i32]) {
    // Sorteer eerst op basis van reguliere algoritme voor cijfers.
    // Algemener voor bit: cijfer/digit.
    for bit in 0..31 {
        let (nullen, enen): (Vec<_>, Vec<_>) =
            rij.iter().partition(|&&item| (item >> bit) & 1 == 0);
        merge(&nullen, &enen, rij);
        dbg!();
    }
    // Sorteer nog even het speciale geval: het teken-bit.
    // Verdeel waarden op basis van negatief vs. positief teken (bit 32).
    let (negatieven, positieven): (Vec<_>, Vec<_>) = rij.iter().partition(|&&x| x < 0);
    merge(&negatieven, &positieven, rij);
}

#[cfg(test)]
mod tests {
    use super::*;
    const RIJ_GESORTEERD: [i32; 10] = [-802, -90, -17, 2, 2, 24, 45, 66, 75, 170];

    #[test]
    fn test_radix() {
        let mut rij = [170, 45, 75, -90, -802, 24, 2, 66, -17, 2];
        radix_sort(&mut rij);
        assert_eq!(rij, RIJ_GESORTEERD);
    }
}
