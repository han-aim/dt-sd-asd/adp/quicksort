/// Hashtabel met strings als sleutels en positieve gehele getallen als waarden.
/// Met linear probing, zonder wraparound.
pub struct Hashtabel {
    onderliggende_rij: Vec<Option<(String, usize)>>,
    // 2 * aantal items
    belading: usize,
}

fn _hash(sleutel: &str, deler: usize) -> usize {
    let mut som: usize = 0;
    for karakter in sleutel.bytes() {
        dbg!(karakter);
        som += usize::from(karakter);
    }
    return som % deler;
}

impl Hashtabel {
    #[must_use]
    pub fn new() -> Self {
        let onderliggende_rij = vec![None; 53];
        return Hashtabel {
            onderliggende_rij,
            belading: 0,
        };
    }

    fn hash(&self, sleutel: &str) -> usize {
        return _hash(sleutel, self.onderliggende_rij.len());
    }

    fn rehash(&mut self, index: usize) {
        let mut onderliggende_rij_nieuw: Vec<Option<(String, usize)>> =
            Vec::with_capacity(index + 1);
        onderliggende_rij_nieuw.resize(index + 1, None);
        // Verwijder ieder item uit oorspronkelijke onderliggende rij, zodra het in de nieuwe onderliggende rij staat.
        self.onderliggende_rij.drain(..).for_each(|optie_waarde| {
            if let Some((ref sleutel, _)) = optie_waarde {
                let index_nieuw = _hash(sleutel, onderliggende_rij_nieuw.len());
                onderliggende_rij_nieuw[index_nieuw] = optie_waarde;
            };
        });
        self.onderliggende_rij = onderliggende_rij_nieuw;
    }

    pub fn plaats(&mut self, sleutel: &str, waarde: usize) -> Option<usize> {
        let mut index = self.hash(sleutel);
        // Zie het commentaar gemarkeerd met *.
        if index >= self.onderliggende_rij.len() || self.belading >= self.onderliggende_rij.len() {
            self.rehash(self.belading * 2);
            index = self.hash(sleutel);
        }
        let mut probe = 0;

        while index + probe < self.onderliggende_rij.len() {
            if let None = &self.onderliggende_rij[index + probe] {
                self.onderliggende_rij[index + probe] = Some((sleutel.to_string(), waarde));
                // *: Micro-optimalisatie: tel 2 erbij op in plaats van vermenigvuldigen, delen en afronden om de load factor te berekenen.
                self.belading += 2;
                return Some(index + probe);
            }
            probe += 1;
        }
        // Wrap-around was nodig, en dat ondersteunt deze hashtabel niet.
        // Dit kan alleen gebeuren als de tweede helft van de rij helemaal vol zit en de eerste helft helemaal leeg is.
        return None;
    }

    #[must_use]
    #[allow(clippy::missing_panics_doc)]
    pub fn krijg(&self, sleutel: &str) -> &Option<(String, usize)> {
        let index = self.hash(sleutel);
        let mut probe = 0;
        while index + probe < self.onderliggende_rij.len() {
            if let ref item @ Some((ref sleutel_gevonden, _)) =
                &self.onderliggende_rij[index + probe]
            {
                if sleutel == sleutel_gevonden {
                    return item;
                }
            }
            probe += 1;
        }
        return &None;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_hash() {
        let mut hashtabel = Hashtabel::new();
        hashtabel.plaats("test", 123);
        assert_eq!(hashtabel.krijg("test"), &Some((String::from("test"), 123)));
    }

    #[test]
    fn test_hash_2() {
        let mut hashtabel = Hashtabel::new();
        hashtabel.plaats("test", 123);
        hashtabel.plaats("etst", 124);
        assert_eq!(hashtabel.krijg("etst"), &Some((String::from("etst"), 124)));
        assert_eq!(hashtabel.krijg("test"), &Some((String::from("test"), 123)));
    }

    #[test]
    fn test_hash_3() {
        let mut hashtabel = Hashtabel::new();
        hashtabel.plaats("test", 123);
        hashtabel.plaats("etst", 124);
        assert_eq!(hashtabel.krijg("blabla"), &None);
        assert_eq!(hashtabel.krijg("blablabla"), &None);
    }
}
