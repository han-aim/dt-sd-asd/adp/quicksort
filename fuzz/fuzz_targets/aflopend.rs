#![no_main]
#![feature(is_sorted)]

use libfuzzer_sys::fuzz_target;
use quicksorter::quicksort;

fuzz_target!(|data: &[u8]| {
    let mut data2 = data.to_owned();
    quicksort::<_, true>(&mut data2);
    data2.reverse();
    assert!(data2.is_sorted());
});
